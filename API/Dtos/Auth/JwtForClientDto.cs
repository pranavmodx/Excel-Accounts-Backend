namespace API.Dtos.Auth
{
    public class JwtForClientDto
    {
        public string Token { get; set; }
    }
}